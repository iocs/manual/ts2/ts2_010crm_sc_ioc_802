#
# Module: essioc
#
require essioc

#
# Module: cabtr
#
require cabtr


#
# Setting STREAM_PROTOCOL_PATH
#
epicsEnvSet(STREAM_PROTOCOL_PATH, "${cabtr_DB}")


#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")

#
# Device: TS2-010CRM:Cryo-TC-002
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_monitor.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TC-002, IPADDR = ts2-cabtr-02.tn.esss.lu.se, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-064
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-064, CONTROLLER = TS2-010CRM:Cryo-TC-002, CHANNEL = 1, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-024
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-024, CONTROLLER = TS2-010CRM:Cryo-TC-002, CHANNEL = 2, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-013
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-013, CONTROLLER = TS2-010CRM:Cryo-TC-002, CHANNEL = 3, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-041
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-041, CONTROLLER = TS2-010CRM:Cryo-TC-002, CHANNEL = 4, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-031
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-031, CONTROLLER = TS2-010CRM:Cryo-TC-002, CHANNEL = 5, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-007
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-007, CONTROLLER = TS2-010CRM:Cryo-TC-002, CHANNEL = 6, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-008
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-008, CONTROLLER = TS2-010CRM:Cryo-TC-002, CHANNEL = 7, POLL = 1000")

#
# Device: TS2-010CRM:Cryo-TE-009
# Module: cabtr
#
iocshLoad("${cabtr_DIR}/cabtr_te.iocsh", "DEVICENAME = TS2-010CRM:Cryo-TE-009, CONTROLLER = TS2-010CRM:Cryo-TC-002, CHANNEL = 8, POLL = 1000")
