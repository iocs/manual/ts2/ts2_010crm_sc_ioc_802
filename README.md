# IOC to control TS2-010CRM:Cryo-TC-002

## Used modules

*   [cabtr](https://gitlab.esss.lu.se/e3/wrappers/communication/e3-cabtr.git)


## Controlled devices

*   TS2-010CRM:Cryo-TC-002
    *   TS2-010CRM:Cryo-TE-064
    *   TS2-010CRM:Cryo-TE-024
    *   TS2-010CRM:Cryo-TE-013
    *   TS2-010CRM:Cryo-TE-041
    *   TS2-010CRM:Cryo-TE-031
    *   TS2-010CRM:Cryo-TE-007
    *   TS2-010CRM:Cryo-TE-008
    *   TS2-010CRM:Cryo-TE-009
